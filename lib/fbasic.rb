require "fbasic/version"
require 'digest'


module FBasic
  class Hashes
    
    def self.md5(file_location)
      puts Digest::MD5.file(file_location).hexdigest 
    end
    
    def self.sha1(file_location)
      puts Digest::SHA1.file(file_location).hexdigest 
    end
  
  end  
  
  class Search
    
    def self.file(term, file)
      puts `grep #{term} #{file}`
    end
    
    def self.directory(term, dir)
      puts `grep -R #{term} #{dir}`
    end
  end
  
  class User
    def self.all
      puts `dscl . list /Users | grep -v '^_'`
    end
    
    def self.logon(user)
      puts `last #{user}`
    end

    def self.hidden
      puts `dscl . -ls /Users`
    end
  end
  
  class Partition
    def self.all
      puts `diskutil list`
    end
    
    def self.info(disk)
      puts `diskutil info #{disk}`
      
    end
  end
  
  class File
    def self.info(location)
      puts `mdls #{location}`
    end
    
    def self.basic(location)
      puts `ls -lh #{location}`
    end
  end
end

module Def
  class Why
    def self.hash
      puts 'The method provides a unique identifier to the file that you direct it to.
      Hashing is a method that forensic analysts use to verify that
      data has been untouched from the beginning to the end of the
      examination process. If you hash a file at the beginning at end of the
      process and receive the same result, then you know that the data has
      been unchanged.'
    end
    
    def self.search
      puts 'This method provides a way of searching through files or directories.
      This is useful if you are searching for a certain term provided to you as a keyword to look for.'
    end
    
    def self.user
      puts 'This method provides way to see all the users including hidden ones of a computer.
      It is important to know who has logged on and a what times they have done so to keep a timeline
      of the computer usage.'
    end
    
    def self.partition
      puts 'This method allows for the viewing of partitions on a computer.  Perhaps the necessary information
      is not on the partition you are searching through. There would be no way of knowing unless you check all the partitions.'
    end
    
    def self.file
      puts 'The method gives information on files.  It is useful to know when a file whas last accessed
      and the priveleges on a file.'
    end
    
  end
end