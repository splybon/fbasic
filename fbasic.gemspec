# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fbasic/version'

Gem::Specification.new do |spec|
  spec.name          = "fbasic"
  spec.version       = Fbasic::VERSION
  spec.authors       = ["Scott Plybon"]
  spec.email         = ["sjplybon@gmail.com"]

  spec.summary       = %q{A basic forensic tool}
  spec.description   = %q{This is designed to help teach people how to use basic forensic analysis}
  spec.homepage      = "https://bitbucket.org/splybon/fbasic"

  spec.files         = `git ls-files`.split("\n")
  spec.executables   = ["fbasic"]
  spec.require_paths = ["lib"]

end
