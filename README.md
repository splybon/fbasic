# Fbasic

Fbasic is an open source, linux based, command line digitial forensic tool.

## Installation
### To install into a rails app
Add this line to your application's Gemfile:

```
gem 'quint_task', git: 'git@bitbucket.org:splybon/fbasic.git'
```

And then execute:

    $ bundle

### To install from command line
```
$ git clone https://splybon@bitbucket.org/splybon/fbasic.git
```
```
$ cd fbasic
```
The below command will create pkg/fbasic-0.1.0.gem
```
$ rake install
```



## Usage

This is used as a command line tool only in unix systems.  The initial command is always fbasic.

### Hash  
1. md5  (hashes file in md5)  
  ```
    $ fbasic hash md5 /path-to-file
  ```
2. sha1 (hashes file in sha1)  
  ```
  $ fbasic hash sha1 /path-to-file
  ```
### Search  
1. file  (searches a file for a specific term)  
  ```
  $ fbasic search search_term /path-to-file
  ```
2. directory  (can search in a directory)  
  ```
  $ fbasic search search_term /path-to-file --dir
  ```
### User  
1. all (shows all users)  
  ```
  $ fbasic user all
  ```
2. logon (shows specific user logon times)  
  ```
  $ fbasic user logon specific_user
  ```
3. hidden (shows all users even hidden ones)  
  ```
  $ fbasic user hidden
  ```
### Partition  
1. all (shows all partitions)  
  ```
  $ fbasic partition all
  ```
2. info (shows info on a specific partition)  
  ```
  $ fbasic partition info specific_partition
  ```
### File  
1. info (shows detailed information on specific file)  
  ```
  $ fbasic file info /path-to-file
  ```
2. basic (shows less detailed information on a specific file)  
  ```
  $ fbasic file basic /path-to-file
  ```


## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.com/splybon/fbasic.